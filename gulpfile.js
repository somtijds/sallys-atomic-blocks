//var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();

gulp.task('sass', function () {
    return gulp.src('./assets/src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./assets/src/css'));
  });

gulp.task('css', function () {
    var plugins = [
        autoprefixer({browsers: ['last 1 version']}),
        cssnano()
    ];
    return gulp.src('./assets/src/css/*.css')
        .pipe(sourcemaps.init())
        .pipe(postcss(plugins))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./assets/dist/css'))
});

//Gulp Watch syntax has changed from 3.x in 4.x
gulp.task('watch', function() {
    gulp.watch('./assets/src/scss/**/*.scss', gulp.series('css', 'sass'));
    gulp.watch('./assets/dist/css/*.css').on('change', browserSync.reload);
    gulp.watch('./**/*.php').on('change', browserSync.reload);
});