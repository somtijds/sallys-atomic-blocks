<?php
/**
 * Sally's Atomic Blocks Theme Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sallys-atomic-blocks
 */

add_action( 'wp_enqueue_scripts', 'sallys_atomic_blocks_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function sallys_atomic_blocks_enqueue_styles() {
	wp_enqueue_style( 'atomic-blocks-theme-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'sallys-atomic-blocks-style',
		get_stylesheet_directory_uri() . '/assets/dist/css/sallys-atomic-blocks.css',
		array( 'atomic-blocks-theme-style' ),
        false

	);
}

function sallys_atomic_blocks_post_media() {
	global $post;

	if ( has_post_thumbnail() ) {
		
		$page_template = get_post_meta( $post->ID, '_wp_page_template', true );

		if ( $page_template == 'templates/template-wide-image.php' 
			|| $page_template == 'templates/template-home.php'  ) {
			$featured_image = get_the_post_thumbnail_url( $post, 'atomic-blocks-featured-image-wide' );
		} else {
			$featured_image = get_the_post_thumbnail_url( $post, 'atomic-blocks-featured-image' );
		}

		// Otherwise get the featured image
		echo '<div class="featured-image">';
			if ( is_single() ) { ?>
				<img src="<?php echo esc_url( $featured_image ); ?>" alt="<?php the_title_attribute(); ?>" />
				<?php } else { ?>
				<a href="<?php the_permalink(); ?>" rel="bookmark"><img src="<?php echo $featured_image; ?>" alt="<?php the_title_attribute(); ?>" /></a>
			<?php }
		echo '</div>';

	}
}

add_action('wp_head','sallys_mt_font_tracking_code');
function sallys_mt_font_tracking_code() {
    ?>
    <script type="text/javascript">
    var MTUserId='ce37c023-59b5-472e-b8e0-ca1d005c4269';
    var MTFontIds = new Array();

    // MTFontIds.push("1475510"); // Avenir® W02 45 Book
    // MTFontIds.push("1475546"); // Avenir® W02 85 Heavy
    // MTFontIds.push("1475508");
    // MTFontIds.push("1475512");
    // MTFontIds.push("1475544");
    // MTFontIds.push("1475548");
       MTFontIds.push("5687178");
       MTFontIds.push("5687390");

    (function() {
        var mtTracking = document.createElement('script');
        mtTracking.type='text/javascript';
        mtTracking.async='true';
        mtTracking.src='<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/dist/fonts/mtiFontTrackingCode.js'); ?>';

        (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(mtTracking);
    })();
    </script>
    <?php
}

