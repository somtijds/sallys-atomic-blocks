<?php
/**
 * Single service
 **/
?>

<article <?php post_class( "grid__item client-grid__item"); ?>>
	<div class="grid__item-inner">
        <?php
            if ( function_exists('get_field') ) {
                $url = get_field('client_url');
            }
            $title = get_the_title();
        ?>
        <?php if ( ! empty( $url ) ) : ?>
        <a href="<?php echo esc_url( $url ); ?>"
           title="<?php echo esc_attr( sprintf( __('Webseite von %s öffnen','sallys'), $title ) ); ?>"
           target="_blank">
        <?php endif; ?>
		    <img alt="<?php echo esc_attr( sprintf( __('Logo-Bild %s','sallys'), $title ) ); ?>"
                 title="<?php esc_attr_e( $title ); ?>"
                 src="<?php echo esc_url( get_the_post_thumbnail_url(get_the_id(),'medium') ); ?>">
        <?php if ( ! empty( $url ) ) : ?>
        </a>
        <?php endif; ?>
	</div>
</article>