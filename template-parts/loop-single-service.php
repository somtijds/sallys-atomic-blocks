<?php
/**
 * Single service
 **/
?>

<article <?php post_class( "grid__item services-grid__item"); ?>>
	<div class="grid__item-inner">
		<span><?php the_title(); ?></span>
	</div>
</article>