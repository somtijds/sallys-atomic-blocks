<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Atomic Blocks
 */
?>

	</div><!-- #content -->
</div><!-- #page .container -->

<?php
	$blog_section = get_theme_mod( 'atomic_blocks_portfolio_blog_section', 'enabled' );

	if ( $blog_section == 'enabled' ) {

	wp_reset_query();

	if ( is_page_template( 'templates/template-portfolio-block.php' ) ||
 		is_page_template( 'templates/template-portfolio-carousel.php' ) ||
		is_page_template( 'templates/template-portfolio-grid.php' ) ||
		is_page_template( 'templates/template-portfolio-masonry.php' ) ) { ?>
<div class="blog-section">
	<div class="container">
		<?php
			$blog_section_title = get_theme_mod( 'atomic_blocks_portfolio_blog_text', esc_html__( 'Latest from the blog', 'atomic-blocks' ) );

			if ( $blog_section_title ) {
				echo '<h3>' . $blog_section_title . '</h3>';

				$post_count = wp_count_posts()->publish;

				if ( $post_count > 3 ) {
					echo '<a class="view-all-posts" href="' . esc_url( get_permalink( get_option( 'page_for_posts' ) ) ) . '"><i class="fa fa-file-text-o"> </i>' . esc_html__( 'View All', 'atomic-blocks' ) . '</a>';
				}
			}
		?>

		<div class="blog-section-posts">
		<?php
			$blog_list_args = array(
                'posts_per_page' => 3
            );
            $blog_list_posts = new WP_Query( $blog_list_args );
	        ?>

	        <?php
	        if ( $blog_list_posts->have_posts() ) :
	        while( $blog_list_posts->have_posts() ) : $blog_list_posts->the_post() ?>
	            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	                <?php if ( has_post_thumbnail() ) { ?>
	                    <div class="featured-image"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'atomic-portfolio' ); ?></a></div>
	                <?php } ?>

	                <?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>

					<?php atomic_blocks_post_byline(); ?>
	            </article><!-- #post-## -->
	        <?php
				endwhile;
				endif;
				wp_reset_postdata();
			?>
		</div><!-- .blog-section-posts -->
    </div><!-- .container -->
</div><!-- .blog-section -->
<?php } } ?>

<?php
	// Related Posts
	if ( class_exists( 'Jetpack_RelatedPosts' ) && is_singular( 'post' ) ) {
		echo '<div class="related-post-wrap">';
			echo '<div class="container">';
				echo do_shortcode( '[jetpack-related-posts]' );
			echo '</div>';
		echo '</div>';
	}
?>
<footer id="kontakt" class="contact-details">
	<div class="container container--header-wrapper">
		<h2 class="contact-details__header">
			<?php _e( 'Kontakt', 'sallys-atomic-blocks' ); ?>
		</h2>
	</div>
	<div class="container container--footer">
		
		<?php if ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) ) : ?>
		<div class="footer-widgets">
			<?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
				<div class="footer-column">
                    <svg class="footer__icon" viewBox="0 0 48 32" xmlns="http://www.w3.org/2000/svg"><g fill="#FEFEFE" fill-rule="evenodd"><path d="M24 21.5l-6-5-17 14c.7.5 1.5.9 2.4.9h41.1c1 0 1.8-.4 2.4-1l-17-13.8-6 5z"/><path d="M46.9 1c-.6-.6-1.5-.9-2.4-.9h-41C2.4.1 1.6.4 1 1l23 18.8L46.9 1zM0 29l17-13.3L0 2zM31 15.7L48 29V2z"/></g></svg>
					<?php dynamic_sidebar( 'footer-1' ); ?>
				</div>
			<?php } ?>

			<?php if ( is_active_sidebar( 'footer-3' ) ) { ?>
				<div class="footer-column">
                    <svg class="footer__icon" viewBox="0 0 35 35" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><path id="a" d="M0 0h35v35H0z"/></defs><g fill="none" fill-rule="evenodd"><mask id="b" fill="#fff"><use xlink:href="#a"/></mask><path d="M35 27.6c0 .6-.1 1-.5 1.5l-5 4.9c-.2.2-.5.5-.8.6a3.6 3.6 0 0 1-1.8.4 17 17 0 0 1-6.1-1.4 32.3 32.3 0 0 1-11.3-8 40.6 40.6 0 0 1-7-9.3A23.3 23.3 0 0 1 .1 10 9.2 9.2 0 0 1 0 7.4a3.6 3.6 0 0 1 1-2L6 .5C6.2.2 6.6 0 7 0c.3 0 .6 0 .9.3l.6.7 4 7.5c.2.4.3.8.2 1.3-.1.5-.4.9-.7 1.2l-1.8 1.8-.1.2v.3c0 .5.2 1.1.6 1.8a24 24 0 0 0 4 5.1 24.4 24.4 0 0 0 5.1 4l1.4.6h.7l.2-.1 2.1-2.2c.5-.4 1-.6 1.6-.6.4 0 .7.1 1 .3l7.2 4.2c.5.3.8.7.9 1.2" fill="#FFF" mask="url(#b)"/></g></svg>
					<?php dynamic_sidebar( 'footer-3' ); ?>
				</div>
			<?php } ?>
		</div>
		<?php endif; ?>
	</div>
</footer>


<footer id="colophon" class="site-footer">
	<div class="container container--colophon">
		

		<div class="footer-bottom">
			<div class="footer-tagline">
				<div class="site-info">
					<?php echo atomic_blocks_filter_footer_text(); ?>
				</div>
			</div><!-- .footer-tagline -->

			<?php if ( has_nav_menu( 'social' ) ) { ?>
				<nav class="social-navigation">
					<?php wp_nav_menu( array(
						'theme_location' => 'social',
						'depth'          => 1,
						'fallback_cb'    => false
					) );?>
				</nav><!-- .social-navigation -->
			<?php } ?>
			<?php $privacy_page = get_option( 'wp_page_for_privacy_policy '); ?>
			<?php if ( ! empty( $privacy_page ) && 'publish' === get_post_status( $privacy_page ) ) : ?>
			<div class="impressum-and-or-privacy">
				<span class="link-to-privacy-statement">
					<a href="<?php echo esc_url( get_the_permalink( $privacy_page ) ); ?>" title="<?php echo esc_attr( get_the_title( $privacy_page ) ); ?>"><?php echo esc_html( get_the_title( $privacy_page ) ); ?></a>
				</span>
			</div>
			<?php endif; ?>
		</div><!-- .footer-bottom -->
	</div><!-- .container -->
</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
